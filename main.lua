_G.MALFOR = RegisterMod("Malformations", 1)

MALFOR.MalformationChance = 100 -- the chance an enemy gets replaced by a malformation

--[[ DOCUMENTATION

functions:

-- MALFOR.SpawnMalformation(pos, difficulty, bodyid, headid, projectilepatternid, spawnerentity)
returns Entity
Spawns a malformation.
- pos (Vector): spawn position of the malformation
- difficulty (Number, default is based on floor difficulty): determines the difficulty of the enemy, higher difficulty means more difficult behaviour and stats
- bodyid (MalformationBody Enum (Integer), default is randomized): determines which body the malformation will have
- headid (MalformationHead Enum (Integer), default is randomized): determines which head the malformation will have
- projectilepatternid (MalformationProjectilePattern Enum (Integer), default is randomized and based on difficulty): determines the projectile pattern of the malformation's projectile attack
- spawnerentity (Entity, default = nil): for if the malformation was spawned by another entity

-- MALFOR.FireProjectile(npc, velocity, shotspeedmult)
returns EntityProjectile
Fires a projectile from a malformation, taking into account the stats of the malformation including projectile variant and flags.
- npc (Entity): put in the malformation in question, won't work for other entities
- velocity (Vector): determines in which direction the projectile goes, doesn't determine the speed
- shotspeedmult (Number, default = 1): multiplies the shotspeed of the projectile by the specified number

-- MALFOR.HeadPosHelper(default, list)
returns Table with Vectors
shorter way to write all the positions for the head on certain frames
- default (String): either "X" or "Y", if the list contains numbers it'll convert it to either Vector(number, 0) or Vector(0, number)
- list (Table): with the key you specify for which frame the head position is. as value put in a number to convert it either to Vector(number, 0) or Vector(0, number), or put in {x,y} to convert it to Vector(x,y)

-- MALFOR.AddMovement(name, func)
adds another function to the MALFOR.MOVEMENT table
- name (String): the way the function can be accessed in that table
- func (Function): this function can use the npc parameter, handles the velocity of that npc

-- MALFOR.AddProjectilePattern(projectilepattern)
returns ProjectilePatternId
adds a new projectilepattern to the list of which malformations randomly choose from based on difficulty
- projectilepattern (Table): {
	difficulty (Number): determines the difficulty of the projectilepattern
	attack (Function): this function can use the npc parameter, handles the projectiles fired off, using MALFOR.FireProjectile() is recommended
}

-- MALFOR.AddBodySpritehandler(name, func)
adds another function to the MALFOR.SPRITEHANDLE.BODY table
- name (String): the way the function can be accessed in that table
- func (Function): this function can use the npc parameter, handles the sprite of the body for that npc

-- MALFOR.AddHeadSpritehandler(name, func)
adds another function to the MALFOR.SPRITEHANDLE.HEAD table
- name (String): the way the function can be accessed in that table
- func (Function): this function can use the npc parameter, handles the sprite of the head for that npc

-- MALFOR.AddBody(body)
returns BodyId
adds another body malformations can choose from
- body (Table): {
	difficulty (Number): determines the difficulty of the body
	anm2 (String): the name of the anm2 file in gfx\monsters\malformationbodies\, no path or .anm2 is needed in the string
	defaultanim (String): the name of the default animation, the body starts out in that animation
	colradius (Number): hitbox size of the body
	headpos (Vector): the offset of the head relative to the body
	
	-- optional attributes
	movement (Function): the function that handles the movement
	spritehandler (Function): the function that handles the body's sprite
	headposhelper (Table with Vectors): put in a table which specifies with it's keys which frames get which offset, MALFOR.HeadPosHelper() helps shortening the table
	entcolclass (EntityCollisionClass Enum): determines collision to entities
	entgridcolclass (EntityGridCollisionClass Enum): determines collision to grid entities
	stationary (Boolean): let's the mod know if the speed stat is used, removes knockback when true
	coldmg (Integer, default = 1): collision damage
	healthmult (Number): hitpoints is multiplied by this number
	speedmult (Number): speed is multiplied by this number
	explosive (Boolean): the enemy explodes on death
	explosivetouch (Boolean): the enemy explodes on touch
}
see the MALFOR.BODY table for examples

-- MALFOR.AddHead(head)
returns HeadId
adds another head malformations can choose from
- head (Table): {
	difficulty (Number): determines the difficulty of the head
	anm2 (String): the name of the anm2 file in gfx\monsters\malformationheads\, no path or .anm2 is needed in the string
	defaultanim (String): the name of the default animation, the head starts out in that animation
	
	-- optional attributes
	spritehandler (Function): the function that handles the head's sprite
	projectiles (Boolean): let's the mod know if the firedelay and shotspeed stat are used
	projvariant (Integer): variant of the projectiles
	projflags (Integer): added flags to the projectiles
	firedelaymult (Number): firedelay is multiplied by this number
	shotspeedmult (Number): shotspeed is multiplied by this number
	explosive (Boolean): the enemy explodes on death
	explosivetouch (Boolean): the enemy explodes on touch
}
see the MALFOR.HEAD table for examples


tables:

-- MALFOR.MOVEMENT
DEFAULT_GROUND_MOVEMENT - used for non-flying enemies. takes the closest path to the player, pathfinds it's way arounds grid entities
DEFAULT_FLYING_MOVEMENT - simple movement towards the player
DRIFTING_FLYING_MOVEMENT - slowly drifts towards the player

-- MALFOR.SPRITEHANDLER.BODY
DEFAULT_WALK - uses "WalkHori" and "WalkVert"
SIMPLE_WALK - uses "Walk"

-- MALFOR.SPRITEHANDLER.HEAD
DEFAULT_ATTACK - uses "Attack"
FACE_DIRECTION - head faces the direction the body is going

]]

-- ENUMS --
do

MalformationBody = {
	NO_BODY = 1,
	DEFAULT_BODY = 2,
	FATTY_BODY = 3,
	CRAZYSPIDER_BODY = 4,
	HANGER_BODY = 5
}

MalformationHead = {
	GAPER_HEAD = 1,
	HORF_HEAD = 2,
	FATTY_HEAD = 3,
	MULLIBOOM_HEAD = 4
}

MalformationProjectilePattern = {
	ONE_PROJECTILE = 1,
	TWO_PROJECTILES_SPREADOUT = 2,
	THREE_PROJECTILES_SPREADOUT = 3,
	FOUR_PROJECTILES_SPREADOUT = 4
}

end
-- MALFORMATION FUNCTIONALITY --

local game = Game()

MALFOR.ENT = {id=Isaac.GetEntityTypeByName("Malformation"), variant=Isaac.GetEntityVariantByName("Malformation")}

MALFOR.SpawnMalformation = function(pos, difficulty, bodyid, headid, projectilepatternid, spawnerentity)
	local ent = Isaac.Spawn(MALFOR.ENT.id, MALFOR.ENT.variant, difficulty or 0, pos, Vector(0,0), spawnerentity)
	if bodyid then ent:GetData().Body = MALFOR.BODY[bodyid] end
	if headid then ent:GetData().Head = MALFOR.HEAD[headid] end
	if projectilepatternid then ent:GetData().ProjectilePattern = MALFOR.PROJECTILEPATTERN[projectilepatternid] end
	return ent
end

-- MOVEMENT --
do

MALFOR.AddMovement = function(name, func)
	MALFOR.MOVEMENT[name] = func
end

MALFOR.MOVEMENT = {
	DEFAULT_GROUND_MOVEMENT = function(npc)
		local data,room,targetpos = npc:GetData(),game:GetRoom(),npc:GetPlayerTarget().Position
		if room:CheckLine(npc.Position, targetpos, 0, 0, false, false) then
			npc.Velocity = (targetpos-npc.Position):Resized(data.Speed*3)
		else
			npc.Pathfinder:FindGridPath(targetpos, data.Speed/2, 0, false)
		end
	end,
	DEFAULT_FLYING_MOVEMENT = function(npc)
		local data,targetpos = npc:GetData(),npc:GetPlayerTarget().Position
		npc.Velocity = (targetpos-npc.Position):Resized(data.Speed*2)
	end,
	DRIFTING_FLYING_MOVEMENT = function(npc)
		local data,targetpos = npc:GetData(),npc:GetPlayerTarget().Position
		npc.Velocity = npc.Velocity*0.95+(targetpos-npc.Position):Resized(data.Speed*0.2)
	end
}

end
-- PROJECTILES --
do

MALFOR.FireProjectile = function(npc, velocity, shotspeedmult)
	local data,shotspeedmult = npc:GetData(),shotspeedmult or 1
	local proj = Isaac.Spawn(EntityType.ENTITY_PROJECTILE, data.ProjVariant, 0, npc.Position, velocity:Resized(data.ShotSpeed+shotspeedmult), npc):ToProjectile()
	proj.ProjectileFlags = data.ProjFlags
	return proj
end

MALFOR.AddProjectilePattern = function(projectilepattern)
	table.insert(MALFOR.PROJECTILEPATTERN, projectilepattern)
	return #MALFOR.PROJECTILEPATTERN
end

MALFOR.PROJECTILEPATTERN = {
	{ -- one projectile spreadout
		difficulty = 0,
		attack = function(npc)
			MALFOR.FireProjectile(npc, (npc:GetPlayerTarget().Position-npc.Position))
		end
	},
	{ -- two projectiles spreadout
		difficulty = 10,
		attack = function(npc)
			for i=1, 2 do
				MALFOR.FireProjectile(npc, (npc:GetPlayerTarget().Position-npc.Position):Rotated((i*20)-30))
			end
		end
	},
	{ -- three projectiles spreadout
		difficulty = 15,
		attack = function(npc)
			for i=1, 3 do
				MALFOR.FireProjectile(npc, (npc:GetPlayerTarget().Position-npc.Position):Rotated((i*20)-40))
			end
		end
	},
	{ -- four projectiles spreadout
		difficulty = 20,
		attack = function(npc)
			for i=1, 3 do
				MALFOR.FireProjectile(npc, (npc:GetPlayerTarget().Position-npc.Position):Rotated((i*20)-50))
			end
		end
	}
}

end
-- SPRITEHANDLER --
do

MALFOR.AddBodySpritehandler = function(name, func)
	MALFOR.SPRITEHANDLER.BODY[name] = func
end

MALFOR.AddHeadSpritehandler = function(name, func)
	MALFOR.SPRITEHANDLER.HEAD[name] = func
end

MALFOR.SPRITEHANDLER = {
	BODY = {
		DEFAULT_WALK = function(npc)
			local sprite = npc:GetSprite()
			if math.abs(npc.Velocity.X) > math.abs(npc.Velocity.Y) then
				sprite.PlaybackSpeed = 1
				if npc.Velocity.X >= 0 and (not sprite:IsPlaying("WalkHori") or sprite.FlipX) then
					sprite:Play("WalkHori", true)
					sprite.FlipX = false
				elseif npc.Velocity.X < 0 and (not sprite:IsPlaying("WalkHori") or not sprite.FlipX) then
					sprite:Play("WalkHori", true)
					sprite.FlipX = true
				end
			elseif math.abs(npc.Velocity.Y) > math.abs(npc.Velocity.X) then
				sprite.PlaybackSpeed = 1
				if npc.Velocity.Y >= 0 and (not sprite:IsPlaying("WalkVert") or sprite.FlipX) then
					sprite:Play("WalkVert", true)
					sprite.FlipX = false
				elseif npc.Velocity.Y < 0 and (not sprite:IsPlaying("WalkVert") or not sprite.FlipX) then
					sprite:Play("WalkVert", true)
					sprite.FlipX = true
				end
			else
				sprite.PlaybackSpeed = 0
			end
		end,
		SIMPLE_WALK = function(npc)
			local sprite,data = npc:GetSprite(),npc:GetData()
			if not sprite:IsPlaying("Walk") then
				sprite:Play("Walk", true)
			end
			sprite.FlipX = npc.Velocity.X < 0
		end,
	},
	HEAD = {
		DEFAULT_ATTACK = function(npc)
			local data = npc:GetData()
			if data.FireDelay == 0 and game:GetRoom():CheckLine(npc.Position, npc:GetPlayerTarget().Position, 0, 0, false, false) then
				data.FireDelay = data.MaxFireDelay
				data.HeadSprite:Play("Attack", true)
			end
			if data.HeadSprite:IsEventTriggered("Attack") then
				data.ProjectilePattern.attack(npc)
			end
			if data.HeadSprite:IsFinished("Attack") then
				data.HeadSprite:Play(data.Head.defaultanim, true)
			end
		end,
		FACE_DIRECTION = function(npc)
			local data = npc:GetData()
			if npc.Velocity.X < 0 then
				data.HeadSprite.FlipX = true
			elseif npc.Velocity.X > 0 then
				data.HeadSprite.FlipX = false
			end
		end
	}
}

MALFOR.HeadPosHelper = function(default, list)
	local newlist = {}
	for i,v in pairs(list) do
		if type(v) == "table" then
			newlist[i] = Vector(v[1],v[2])
		elseif default == "X" then
			newlist[i] = Vector(v,0)
		elseif default == "Y" then
			newlist[i] = Vector(0,v)
		end
	end
	return newlist
end

end
-- BODIES --
do

MALFOR.AddBody = function(body)
	table.insert(MALFOR.BODY, body)
	return #MALFOR.BODY
end

MALFOR.BODY = {
	{ -- NO BODY
		difficulty = 0,
		anm2 = "no_body",
		defaultanim = "Idle",
		colradius = 13,
		movement = MALFOR.MOVEMENT.DRIFTING_FLYING_MOVEMENT,
		headpos = Vector(0,-7),
		entgridcolclass = EntityGridCollisionClass.GRIDCOLL_WALLS
	},
	{ -- DEFAULT BODY
		difficulty = 10,
		anm2 = "default_body",
		defaultanim = "WalkVert",
		colradius = 13,
		movement = MALFOR.MOVEMENT.DEFAULT_GROUND_MOVEMENT,
		spritehandler = MALFOR.SPRITEHANDLER.BODY.DEFAULT_WALK,
		headpos = Vector(0,-7)
	},
	{ -- FATTY BODY
		difficulty = 10,
		anm2 = "fatty_body",
		defaultanim = "WalkVert",
		colradius = 15,
		movement = MALFOR.MOVEMENT.DEFAULT_GROUND_MOVEMENT,
		spritehandler = MALFOR.SPRITEHANDLER.BODY.DEFAULT_WALK,
		headpos = Vector(0,-20),
		headposhelper = MALFOR.HeadPosHelper("Y", {[12]=-13,[13]=-14,[14]=-15,[15]=-17,[16]=-18,[17]=-19,[30]=-13,[31]=-14,[32]=-15,[33]=-17,[34]=-18,[35]=-19}),
		healthmult = 1.5,
		speedmult = 2/3
	},
	{ -- CRAZYSPIDER BODY
		difficulty = 25,
		anm2 = "crazyspider_body",
		defaultanim = "Idle",
		colradius = 13,
		headpos = Vector(0,-20),
		headposhelper = MALFOR.HeadPosHelper("Y", {[2]=-19,[3]=-18,[4]=-19,[5]=-18}),
		movement = MALFOR.MOVEMENT.DEFAULT_GROUND_MOVEMENT,
		spritehandler = MALFOR.SPRITEHANDLER.BODY.SIMPLE_WALK,
		speedmult = 1.5
	},
	{ -- HANGER BODY
		difficulty = 5,
		anm2 = "hanger_body",
		defaultanim = "Idle",
		colradius = 13,
		headpos = Vector(0,-10),
		movement = MALFOR.MOVEMENT.DEFAULT_FLYING_MOVEMENT,
		entgridcolclass = EntityGridCollisionClass.GRIDCOLL_WALLS
	}
}

end
-- HEADS --
do

MALFOR.AddHead = function(head)
	table.insert(MALFOR.HEAD, head)
	return #MALFOR.HEAD
end

MALFOR.HEAD = {
	{ -- GAPER HEAD
		difficulty = 0,
		anm2 = "gaper_head",
		defaultanim = "Head"
	},
	{ -- HORF HEAD
		difficulty = 10,
		anm2 = "horf_head",
		defaultanim = "Shake",
		projectiles = true,
		spritehandler = MALFOR.SPRITEHANDLER.HEAD.DEFAULT_ATTACK
	},
	{ -- FATTY HEAD
		difficulty = 5,
		anm2 = "fatty_head",
		defaultanim = "Head",
		projectiles = true,
		firedelaymult = 2,
		spritehandler = MALFOR.SPRITEHANDLER.HEAD.DEFAULT_ATTACK
	},
	{ -- MULLIBOOM HEAD
		difficulty = 15,
		anm2 = "mulliboom_head",
		defaultanim = "Head",
		explosive = true,
		explosivetouch = true,
		spritehandler = MALFOR.SPRITEHANDLER.HEAD.FACE_DIRECTION
	}
}

end
-- MALFORMATION LOGIC --
do

MALFOR:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function() -- replaces enemies in the room into malformations
	for _,ent in ipairs(Isaac.GetRoomEntities()) do
		if ent:IsEnemy() and not ent:IsBoss() and ent.Type ~= EntityType.ENTITY_FIREPLACE and math.random()*100 <= MALFOR.MalformationChance then
			local bodyid
			if ent:IsFlying() then
				local flyingbodies = {}
				for i,body in ipairs(MALFOR.BODY) do
					if body.entgridcolclass == EntityGridCollisionClass.GRIDCOLL_WALLS then
						table.insert(flyingbodies, i)
					end
				end
				bodyid = flyingbodies[math.random(#flyingbodies)]
			else
				bodyid = math.random(#MALFOR.BODY)
			end
			MALFOR.SpawnMalformation(ent.Position, nil, bodyid)
			ent:Remove()
		end
	end
end)

MALFOR:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, npc)
	if npc.Variant == MALFOR.ENT.variant then
		local sprite,data = npc:GetSprite(),npc:GetData()
		
		if not data.Init then -- malformation setup
			data.Init = true
		
			-- difficulty of the malformation based on subtype or the stage
			local stage = game:GetLevel():GetAbsoluteStage()
			if game:IsGreedMode() then 
				stage = stage*2 
			end
			local ballancepoints
			if npc.SubType == 0 then
				ballancepoints = (stage*5)
			else
				ballancepoints = npc.SubType
			end
			
			-- body setup
			if not data.Body then
				data.Body = MALFOR.BODY[math.random(#MALFOR.BODY)]
			end
			data.MaxHitPoints = (8+(ballancepoints/2))*(data.Body.healthmult or 1)
			ballancepoints = ballancepoints-data.Body.difficulty
			data.Size = data.Body.colradius
			sprite:Load("gfx/monsters/malformationbodies/"..data.Body.anm2..".anm2", true)
			sprite:Play(data.Body.defaultanim, true)
			
			-- head setup
			if not data.Head then
				data.Head = MALFOR.HEAD[math.random(#MALFOR.HEAD)]
			end
			ballancepoints = ballancepoints-data.Head.difficulty
			data.HeadSprite = Sprite()
			data.HeadSprite:Load("gfx/monsters/malformationheads/"..data.Head.anm2..".anm2", true)
			data.HeadSprite:Play(data.Head.defaultanim, true)
			
			if not data.Body.stationary then -- incase the enemy doesn't move speed isn't a necessary stat
				data.Speed = 1*(data.Body.speedmult or 1)
			end
			
			-- projectiles setup
			if data.Head.projectiles then
				data.MaxFireDelay = math.ceil(60*(data.Head.firedelaymult or 1))
				data.ShotSpeed = 10*(data.Head.shotspeedmult or 1)
				data.ProjVariant = data.Head.projvariant or 0
				data.ProjFlags = data.Head.projflags or 0
				if not data.ProjectilePattern then
					i = 0
					while i ~= 50 do
						data.ProjectilePattern = MALFOR.PROJECTILEPATTERN[math.random(#MALFOR.PROJECTILEPATTERN)]
						if ballancepoints-data.Head.difficulty >= 0 or data.Head.difficulty == 0 then
							break
						end
						i = i+1
					end
					if i == 50 then
						data.ProjectilePattern = MALFOR.PROJECTILEPATTERN[1]
					end
				end
				ballancepoints = ballancepoints-data.ProjectilePattern.difficulty
			end
			
			-- adjusts the malformation's stats based on difficulty
			local attributes = {"MaxHitPoints", "Size", "Speed", "MaxFireDelay", "ShotSpeed"}
			local validattributes = {}
			for _,v in ipairs(attributes) do
				if data[v] then
					table.insert(validattributes, v)
				end
			end
			while #validattributes ~= 2 do
				table.remove(validattributes, math.random(#validattributes))
			end
			for _,v in ipairs(validattributes) do
				if ballancepoints > 0 then
					data[v] = data[v]*math.min(1+(ballancepoints*0.025), 2)
				elseif ballancepoints < 0 then
					data[v] = data[v]*math.max(1-(ballancepoints*0.0125), 0.5)
				end
			end
			
			-- updates some attributes after the stat changes
			data.MaxHitPoints = math.ceil(data.MaxHitPoints)
			npc.MaxHitPoints = data.MaxHitPoints
			npc.HitPoints = data.MaxHitPoints
			data.FireDelay = data.MaxFireDelay
			
			-- size and shadow setup
			data.ScaleDifference = data.Size/data.Body.colradius
			npc:SetSize(data.Size, Vector(1,1), 16)
			npc.Scale = data.ScaleDifference
			data.HeadSprite.Scale = Vector(data.ScaleDifference,data.ScaleDifference)
			data.Shadow = Sprite()
			data.Shadow:Load("gfx/shadow.anm2", true)
			data.Shadow:SetFrame("Shadow", 0)
			data.Shadow.Scale = Vector(data.Size/80,data.Size/80)
			
			-- applying some of the attributes to the malformation
			npc.CollisionDamage = data.Body.coldmg or 1
			npc.EntityCollisionClass = data.Body.entcolclass or EntityCollisionClass.ENTCOLL_ALL
			npc.GridCollisionClass = data.Body.entgridcolclass or EntityGridCollisionClass.GRIDCOLL_GROUND
			if data.Body.stationary then
				npc:AddEntityFlags(EntityFlag.FLAG_NO_KNOCKBACK | EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK)
			end
		end
		
		-- keeping track of when the malformation can fire
		if data.FireDelay and data.FireDelay ~= 0 then
			data.FireDelay = data.FireDelay-1
		end
		
		-- activates movement and sprite changes every on update
		if data.Body.movement then
			data.Body.movement(npc)
		end
		if data.Body.spritehandler then
			data.Body.spritehandler(npc)
		end
		
		-- keeps the head's sprite playing
		data.HeadSprite:Update()
		data.HeadSprite:LoadGraphics()
		if data.Head.spritehandler then
			data.Head.spritehandler(npc)
		end
	end
end, MALFOR.ENT.id)

MALFOR:AddCallback(ModCallbacks.MC_POST_NPC_RENDER, function(_, npc)
	local data,sprite = npc:GetData(),npc:GetSprite()
	if npc.Variant == MALFOR.ENT.variant and data.Body and data.Init then
		local renderpos = Isaac.WorldToScreen(npc.Position)
		-- determines render position of the head
		if data.Body.headposhelper then
			local frame = sprite:GetFrame()
			if data.Body.headposhelper[frame] then
				renderpos = renderpos+(data.Body.headposhelper[frame]*data.ScaleDifference)
			else
				renderpos = renderpos+(data.Body.headpos*data.ScaleDifference)
			end
		else
			renderpos = renderpos+(data.Body.headpos*data.ScaleDifference)
		end
		
		if data.Shadow then -- renders the shadow
			data.Shadow:Render(Isaac.WorldToScreen(npc.Position), Vector(0,0), Vector(0,0))
		end
		
		-- renders the body after the shadow
		sprite:Render(Isaac.WorldToScreen(npc.Position), Vector(0,0), Vector(0,0))
		
		-- renders the head at last
		data.HeadSprite.Color = sprite.Color
		data.HeadSprite:Render(renderpos, Vector(0,0), Vector(0,0))
	end
end, MALFOR.ENT.id)

MALFOR:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, ent, dmg, flag, src, invuln)
	local data = ent:GetData()
	if ent.Variant == MALFOR.ENT.variant and data.Head and data.Init then
		if ent.HitPoints-dmg <= 0 then
			if (data.Head.explosive or data.Body.explosive) and not data.exploded then
				data.exploded = true
				Isaac.Explode(ent.Position, ent, 20)
				ent:Kill()
			end
		end
	end
end, MALFOR.ENT.id)

MALFOR:AddCallback(ModCallbacks.MC_PRE_NPC_COLLISION, function(_, npc, collider)
	if npc:GetData().Head and (npc:GetData().Head.explosivetouch or npc:GetData().Body.explosivetouch) and collider.Type == EntityType.ENTITY_PLAYER and not npc:GetData().exploded then
		Isaac.Explode(npc.Position, npc, 20)
		npc:Kill()
	end
end, MALFOR.ENT.id)

MALFOR:AddCallback(ModCallbacks.MC_POST_RENDER, function()
	math.randomseed(Isaac.GetTime())
end)

end